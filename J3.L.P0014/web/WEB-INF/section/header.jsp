<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand/logo -->
  <a class="navbar-brand" href="/QuizOnline">QuizOnline</a>

  <!-- Links -->
  <ul class="navbar-nav">

    <c:if test="${sessionScope.CUR_USER == null}">
        <li class="nav-item">
          <a class="nav-link" href="/QuizOnline/login.jsp">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/QuizOnline/register.jsp">Register</a>
        </li>
    </c:if>

    <c:if test="${sessionScope.CUR_USER != null}">
        <c:if test="${sessionScope.CUR_USER.roleId !=1}">
            <li class="nav-item">
              <a class="nav-link" href="/QuizOnline/MainController?btnAction=viewHistory">History</a>
            </li>
        </c:if>
        <li class="nav-item">
          <a class="nav-link" href="MainController?btnAction=logout">Logout</a>
        </li>
    </c:if>

  </ul>

  <c:if test="${sessionScope.CUR_USER != null}">
      <p class="mt-3 text-right w-100 mr-2" style="color:white">Welcome ${sessionScope.CUR_USER.name}</p>
  </c:if>
</nav>