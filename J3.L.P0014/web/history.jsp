<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html>

<html lang="en">

  <jsp:include page="/WEB-INF/section/meta.jsp">
      <jsp:param name="title" value="QuizOnline - History Page"/>
  </jsp:include>

  <body>
    <jsp:include page="/WEB-INF/section/header.jsp"/>
    <main style="min-height: 100vh">
      <!--Main Content Start--> 

      <div class="container mt-5">
        <form action="MainController">
          <div class="row mb-5">
            <div class="col-3">
              <input name="name" class="form-control" type="text" value="${param.name}" placeholder="Search subject">
            </div>
            <div class="col-3">
              <button type="submit" class="btn btn-primary" name="btnAction" value="viewHistory">Search</button>
            </div>
          </div>
        </form>
      </div>

      <c:if test="${requestScope.HISTORYS != null && requestScope.HISTORYS.size() > 0}">
          <div class="container mt-5">
            <div class="row" style="font-weight: bold">
              <div class="col-1">Subject Name</div>
              <div class="col-1">QuizID</div>
              <div class="col-1">CreatedAt</div>
              <div class="col-1">SubmitAt</div>
              <div class="col-1">Result</div>
              <div class="col-7">Detail</div>
            </div>
            <div class="row"><hr class="w-100"/></div>
              <c:forEach items="${requestScope.HISTORYS}" var="history">
              <div class="row">
                <div class="col-1">
                  ${history.subjectName}
                </div>
                <div class="col-1">
                  ${history.quizId}
                </div>
                <div class="col-1">
                  ${history.createdAt}
                </div>
                <div class="col-1">
                  ${history.submitAt}
                </div>
                <div class="col-1">
                  ${Math.round(history.result * 10) / 10}
                </div>
                <div class="col-7">
                  <c:forEach items="${history.historyDetails}" var="historyDetail" varStatus="count">
                      <strong style="text-decoration: underline">Question ${count.index + 1}: </strong> <br/>
                      <strong>questionContent: </strong> ${historyDetail.questionContent} <br/>
                      <strong>answer1: </strong> ${historyDetail.answer1} <br/>
                      <strong>answer2: </strong> ${historyDetail.answer2} <br/>
                      <strong>answer3: </strong> ${historyDetail.answer3} <br/>
                      <strong>answer4: </strong> ${historyDetail.answer4} <br/>
                      <strong>selectedAnswer: </strong> ${historyDetail.selectedAnswer == -1 ? "No choice" : historyDetail.selectedAnswer} <br/>
                      <strong>answerCorrect: </strong> ${historyDetail.answerCorrect} <br/>
                      <strong>isCorrect: </strong> ${historyDetail.isCorrect} <br/>
                      <hr/>
                  </c:forEach>
                </div>
              </div>
              <div class="row"><hr class="w-100"/></div>
              </c:forEach>
          </div>
      </div>
    </c:if>

    <c:if test="${requestScope.HISTORYS == null || requestScope.HISTORYS.size() == 0}">
        <h1 class="mt-5 text-center mx-auto">No quiz history found</h1>
    </c:if>

    <!--Pagination-->
    <c:if test="${requestScope.HISTORYS != null && requestScope.HISTORYS.size() > 0 
                  && requestScope.NUMBER_OF_HISTORYLINES != null && requestScope.NUMBER_OF_HISTORYLINES > 0}">
          <nav style="display: flex;justify-content: center;">
            <ul class="pagination">
              <c:forEach var = "i" begin = "1" end = "${(requestScope.NUMBER_OF_HISTORYLINES - 1) / 20 + 1}">
                  <li class="page-item">
                    <c:url var="pagingURL" value="" scope="page">
                        <c:param name="btnAction" value="viewHistory"></c:param>
                        <c:param name="page" value="${i}"></c:param>
                        <c:if test="${param.name != null}">
                            <c:param name="name" value="${param.name}"></c:param>
                        </c:if>
                    </c:url>
                    <a class="page-link" href="${pageScope.pagingURL}">${i}</a>
                  </li>
              </c:forEach>
            </ul>
          </nav>
    </c:if>

    <!--Main Content End--> 
  </main>
  <jsp:include page="/WEB-INF/section/footer.jsp"/>
</body>

</html>
