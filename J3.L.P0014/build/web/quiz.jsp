<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html>

<html lang="vi">

  <jsp:include page="/WEB-INF/section/meta.jsp">
      <jsp:param name="title" value="QuizOnline - Quiz Page"/>
  </jsp:include>

  <body>
    <jsp:include page="/WEB-INF/section/header.jsp"/>
    <main style="min-height: 100vh">
      <!--Main Content Start--> 

      <!-- Select Question -->
      <c:if test="${requestScope.QUESTIONS != null && requestScope.QUESTIONS.size() > 0}">
          <div class="container mt-5">
            <h2>Quiz - ${param.subjectId} - ${param.name}</h2>
            <h5 class="text-center text-danger" id="quiztime">${param.time}</h5>
            <form action="MainController" method="POST" id="myForm" >
              <input type="hidden"  name="btnAction" value="submit"/>
              <input type="hidden" name="numberOfQuestions" value="${requestScope.QUESTIONS.size()}"/>
              <input type="hidden" name="quizId" value="${requestScope.QUIZ_ID}"/>
              <c:forEach items="${requestScope.QUESTIONS}" var="question" varStatus="count">
                  <input type="hidden" name="question${count.index}" value="${question.questionId}"/>
                  <input type="hidden" name="answerCorrect${count.index}" value="${question.answerCorrect}"/>
                  <div class="questionBlock bg-light p-3">
                    <p style="font-weight: bold">Question ${count.index + 1}:</p>
                    ${question.questionContent}
                    <hr/>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="answerForQuestion${count.index}" id="radio1${count.index}" value="1">
                      <label class="form-check-label" for="radio1${count.index}">
                        ${question.answer1}
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="answerForQuestion${count.index}" id="radio2${count.index}" value="2">
                      <label class="form-check-label" for="radio2${count.index}">
                        ${question.answer2}
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="answerForQuestion${count.index}" id="radio3${count.index}" value="3">
                      <label class="form-check-label" for="radio3${count.index}">
                        ${question.answer3}
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="answerForQuestion${count.index}" id="radio4${count.index}" value="4">
                      <label class="form-check-label" for="radio4${count.index}">
                        ${question.answer4}
                      </label>
                    </div>
                    <div class="mt-3">
                      <c:if test="${count.index != 0}">
                          <button class="btn btn-primary" type="button" onclick="prev()">Previous</button>
                      </c:if>
                      <c:if test="${count.index != requestScope.QUESTIONS.size() - 1}">
                          <button class="btn btn-primary" type="button" onclick="next()">Next</button>
                      </c:if>
                    </div>
                  </div>
              </c:forEach>
              <button type="submit" class="btn btn-danger float-right mt-3">Submit</button>
            </form>
          </div>
      </c:if>

      <c:if test="${requestScope.QUESTIONS == null || requestScope.QUESTIONS.size() == 0}">
          <h1 class="mt-5 text-center mx-auto">No Question Found</h1>
      </c:if>

      <!--Main Content End--> 
    </main>
    <jsp:include page="/WEB-INF/section/footer.jsp"/>
    <script>
        let index = 0;
        const questionBlocks = document.querySelectorAll(".questionBlock");

        const render = () => {
            questionBlocks.forEach(item => {
                item.style.display = "none";
            });
            questionBlocks[index].style.display = "block";
        }

        render();

        const prev = () => {
            index--;
            render();
        }

        const next = () => {
            index++;
            render();
        }

        let time = parseInt(${param.time}) * 60;
        const quiztime = document.getElementById("quiztime");
        const countdown = setInterval(function () {
            const quiztime = document.getElementById("quiztime");
            quiztime.innerHTML = Math.floor(time / 60) + ":" + time % 60;
            time--;
            if (time < 0) {
                clearInterval(countdown);
                document.getElementById("myForm").submit();
            }
        }, 1000);
    </script>
  </body>

</html>
