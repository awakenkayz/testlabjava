<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html>

<html lang="vi">

  <jsp:include page="/WEB-INF/section/meta.jsp">
      <jsp:param name="title" value="QuizOnline - Home Page"/>
  </jsp:include>

  <body>
    <jsp:include page="/WEB-INF/section/header.jsp"/>
    <main style="min-height: 100vh">
      <!--Main Content Start--> 

      <!-- Select Subject -->
      <c:if test="${requestScope.SUBJECTS != null && requestScope.SUBJECTS.size() > 0}">
          <div class="container mt-5">
            <h2>Click subject to do quiz:</h2>
            <c:forEach items="${requestScope.SUBJECTS}" var="subject">
                <div class="my-4"><a class="btn btn-primary" href="/QuizOnline/DoQuizController?subjectId=${subject.subjectId}&name=${subject.name}&numberOfQuestions=${subject.numberOfQuestions}&time=${subject.time}">${subject.subjectId} - ${subject.name} - ${subject.numberOfQuestions} questions - ${subject.time} minutes</a></div>
            </c:forEach>
          </div>
      </c:if>

      <c:if test="${requestScope.SUBJECTS == null || requestScope.SUBJECTS.size() == 0}">
          <h1 class="mt-5 text-center mx-auto">No Subject Found</h1>
      </c:if>

      <!--Main Content End--> 
    </main>
    <jsp:include page="/WEB-INF/section/footer.jsp"/>
  </body>

</html>
