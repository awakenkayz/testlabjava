<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="en">

  <jsp:include page="/WEB-INF/section/meta.jsp">
      <jsp:param name="title" value="QuizOnline - Login Page"/>
  </jsp:include>

  <body>
    <jsp:include page="/WEB-INF/section/header.jsp"/>
    <main style="min-height: 100vh">
      <!--Main Content Start--> 

      <div class="container">
        <div class="row">
          <form action="MainController" method="POST" class="mx-auto" style="padding: 40px;background: lightblue;margin-top: 100px;">
            <div class="form-group">
              <label for="email">Email</label>
              <input name="email" type="email" class="form-control" id="email" value="${param.email}" placeholder="Enter email" required>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
            </div>
            <p class="text-danger">${requestScope.ERROR}</p>
            <button name="btnAction" value="login" type="submit" class="btn btn-primary">Login</button>
          </form>
        </div>
      </div>

      <!--Main Content End--> 
    </main>
    <jsp:include page="/WEB-INF/section/footer.jsp"/>
  </body>

</html>
