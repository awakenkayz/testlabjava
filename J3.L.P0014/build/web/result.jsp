<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html>

<html lang="en">

  <jsp:include page="/WEB-INF/section/meta.jsp">
      <jsp:param name="title" value="QuizOnline - Result Page"/>
  </jsp:include>

  <body>
    <jsp:include page="/WEB-INF/section/header.jsp"/>
    <main style="min-height: 100vh">
      <!--Main Content Start--> 

      <c:if test="${requestScope.NUMBER_OF_QUESTIONS != null}">
        <div class="container text-center" style="margin-top: 200px">
          <div class="row">
            <div class="col-md-12">
              <h2 class="text-success">
                ${requestScope.NUMBER_OF_CORRECT} correct in total ${requestScope.NUMBER_OF_QUESTIONS} questions
              </h2>
              <h2 class="text-danger">
                Point: ${requestScope.POINT}
              </h2> 
            </div>
          </div>
        </div>
      </c:if>

      <c:if test="${requestScope.NUMBER_OF_CORRECT == null}">
        <h1 class="mt-5 text-center mx-auto">No Result Found</h1>
      </c:if>

      <!--Main Content End--> 
    </main>
    <jsp:include page="/WEB-INF/section/footer.jsp"/>
  </body>

</html>
