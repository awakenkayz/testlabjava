<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>

<!DOCTYPE html>

<html lang="en">

  <jsp:include page="/WEB-INF/section/meta.jsp">
      <jsp:param name="title" value="QuizOnline - Page Not Found"/>
  </jsp:include>
 
  <body>
    <jsp:include page="/WEB-INF/section/header.jsp"/>
    <main style="min-height: 100vh">
      <!--Main Content Start--> 

      <div class="container text-center" style="margin-top: 200px">
        <div class="row">
          <div class="col-md-12">
            <div class="error-template">
              <h1>Oops!</h1>
              <h2>404 Not Found</h2>
              <a href="/QuizOnline" class="btn btn-primary btn-lg mt-5">Take Me Home</a>
            </div>
          </div>
        </div>
      </div>

      <!--Main Content End--> 
    </main>
    <jsp:include page="/WEB-INF/section/footer.jsp"/>
  </body>
 
</html>
