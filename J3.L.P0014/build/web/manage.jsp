<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html>

<html lang="en">

  <jsp:include page="/WEB-INF/section/meta.jsp">
      <jsp:param name="title" value="QuizOnline - Admin Page"/>
  </jsp:include>

  <body>
    <jsp:include page="/WEB-INF/section/header.jsp"/>
    <main style="min-height: 100vh">
      <!--Main Content Start--> 
      <c:set var = "all" scope = "page" value = "-1"/>
      <c:set var = "inActive" scope = "page" value = "0"/>
      <c:set var = "active" scope = "page" value = "1"/>

      <!--Create question-->
      <div class="text-right">
        <button type="button" class="btn btn-success mt-3 mr-4" data-toggle="modal" data-target="#modalCreate">
          Create new question
        </button>
        <jsp:include page="/WEB-INF/ui/modal/create.jsp"/>
      </div>

      <!--Search Section--> 
      <div class="container my-4">
        <form action="ManageQuestionController">
          <div class="row">
            <div class="col-3">
              Select subject:
              <select class="form-control" name="subjectId">
                <c:forEach items="${requestScope.SUBJECTS}" var="subject">
                    <c:if test = "${subject.subjectId == param.subjectId}">
                        <option value="${subject.subjectId}" selected="selected">${subject.name}</option>
                    </c:if>
                    <c:if test = "${subject.subjectId != param.subjectId}">
                        <option value="${subject.subjectId}">${subject.name}</option>
                    </c:if>
                </c:forEach>
              </select>
            </div>
            <div class="col-3">
              Search by name:
              <input type="text" name="questionContent" 
                     class="form-control" id="input" placeholder="Enter question content ..." value="${param.questionContent}">
            </div>
            <div class="col-2">
              Question Status: 
              <select class="form-control" name="questionStatus">
                <c:if test = "${param.questionStatus == null || param.questionStatus == pageScope.all}">
                    <option value="-1" selected="selected">All</option>
                </c:if>
                <c:if test = "${param.questionStatus == pageScope.inActive}">
                    <option value="0" selected="selected">InActive</option>
                </c:if>
                <c:if test = "${param.questionStatus == pageScope.active}">
                    <option value="1" selected="selected">Active</option>
                </c:if>
                <c:if test = "${param.questionStatus != null && param.questionStatus != pageScope.all}">
                    <option value="-1">All</option>
                </c:if>
                <c:if test = "${param.questionStatus != pageScope.inActive}">
                    <option value="0">InActive</option>
                </c:if>
                <c:if test = "${param.questionStatus != pageScope.active}">
                    <option value="1">Active</option>
                </c:if>
              </select>
            </div>
            <div class="col-2">
              Subject Status: 
              <select class="form-control" name="subjectStatus">
                <c:if test = "${param.subjectStatus == null || param.subjectStatus == pageScope.all}">
                    <option value="-1" selected="selected">All</option>
                </c:if>
                <c:if test = "${param.subjectStatus == pageScope.inActive}">
                    <option value="0" selected="selected">InActive</option>
                </c:if>
                <c:if test = "${param.subjectStatus == pageScope.active}">
                    <option value="1" selected="selected">Active</option>
                </c:if>
                <c:if test = "${param.subjectStatus != null && param.subjectStatus != pageScope.all}">
                    <option value="-1">All</option>
                </c:if>
                <c:if test = "${param.subjectStatus != pageScope.inActive}">
                    <option value="0">InActive</option>
                </c:if>
                <c:if test = "${param.subjectStatus != pageScope.active}">
                    <option value="1">Active</option>
                </c:if>
              </select>
            </div>
            <div class="col-2">
              <button type="submit" class="btn btn-primary mt-4">
                Search
              </button>
            </div>
          </div>
        </form>
      </div>


      <!--Result-->
      <c:if test="${requestScope.QUESTIONS != null && requestScope.QUESTIONS.size() > 0 
                    && requestScope.NUMBER_OF_QUESTIONS != null && requestScope.NUMBER_OF_QUESTIONS > 0}">
            <div class="mx-3 my-2">
              <h5>Show ${requestScope.QUESTIONS.size()} in total ${requestScope.NUMBER_OF_QUESTIONS} questions</h5>
            </div>
      </c:if>

      <!-- View, Delete, Update Question -->
      <c:if test="${requestScope.QUESTIONS != null && requestScope.QUESTIONS.size() > 0}">
          <div class="container-fluid">

            <button type="button" class="btn btn-danger mt-3" data-toggle="modal" data-target="#exampleModal">Delete Selected Items</button>
            <jsp:include page="/WEB-INF/ui/modal/delete.jsp"/>

            <table class="table mt-4">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">STT</th>
                  <th scope="col">questionId</th>
                  <th scope="col">questionContent</th>
                  <th scope="col">answer1</th>
                  <th scope="col">answer2</th>
                  <th scope="col">answer3</th>
                  <th scope="col">answer4</th>
                  <th scope="col">answerCorrect</th>
                  <th scope="col">createdAt</th>
                  <th scope="col">subject</th>
                  <th scope="col">status</th>
                  <th scope="col">action</th>
                </tr>
              </thead>
              <tbody>

                <c:forEach items="${requestScope.QUESTIONS}" var="question" varStatus="count">  
                <form action="MainController" method="POST">
                  <tr>
                    <td><input class="mycheck form-check-input mx-0" type="checkbox" name="select" value="${question.questionId}"></td>
                    <td>${count.index + 1}</td>
                    <td><input type="hidden" name="questionId" value="${question.questionId}"/>${question.questionId}
                    </td>
                    <td><textarea style="resize: none" class="form-control" rows="3" name="questionContent" required>${question.questionContent}</textarea></td>
                    <td><textarea style="resize: none" class="form-control" rows="3" name="answer1" required>${question.answer1}</textarea></td>
                    <td><textarea style="resize: none" class="form-control" rows="3" name="answer2" required>${question.answer2}</textarea></td>
                    <td><textarea style="resize: none" class="form-control" rows="3" name="answer3" required>${question.answer3}</textarea></td>
                    <td><textarea style="resize: none" class="form-control" rows="3" name="answer4" required>${question.answer4}</textarea></td>
                    <td><input type="number" class="form-control" value="${question.answerCorrect}" name="answerCorrect" min="1" max="4" step="1" required></td>
                    <td>${question.createdAt}</td>
                    <td>
                      <select class="form-control" name="subjectId">
                        <c:forEach items="${requestScope.SUBJECTS}" var="subject">
                            <c:if test = "${subject.subjectId == question.subjectId}">
                                <option value="${subject.subjectId}" selected="selected">${subject.name}</option>
                            </c:if>
                            <c:if test = "${subject.subjectId != question.subjectId}">
                                <option value="${subject.subjectId}">${subject.name}</option>
                            </c:if>
                        </c:forEach>
                      </select>
                    </td>
                    <td>
                      <select class="form-control" name="questionStatus">
                        <c:if test = "${question.questionStatus == pageScope.active}">
                            <option value="1" selected>Active</option>
                            <option value="0">Inactive</option>
                        </c:if>
                        <c:if test = "${question.questionStatus == pageScope.inActive}">
                            <option value="1">Active</option>
                            <option value="0" selected>Inactive</option>
                        </c:if>
                      </select>
                    </td>
                    <td><button type="submit" name="btnAction" value="updateQuestion" class="btn btn-warning">Update</button></td>
                  </tr>
                </form>
              </c:forEach>

              </tbody>
            </table>
          </div>
      </c:if>

      <c:if test="${requestScope.QUESTIONS == null || requestScope.QUESTIONS.size() == 0}">
          <h1 class="mt-5 text-center mx-auto">No Question Found</h1>
      </c:if>

      <!--Pagination-->
      <c:if test="${requestScope.QUESTIONS != null && requestScope.QUESTIONS.size() > 0 
                    && requestScope.NUMBER_OF_QUESTIONS != null && requestScope.NUMBER_OF_QUESTIONS > 0}">
            <nav style="display: flex;justify-content: center;">
              <ul class="pagination">
                <c:forEach var = "i" begin = "1" end = "${(requestScope.NUMBER_OF_QUESTIONS - 1) / 20 + 1}">
                    <li class="page-item">
                      <c:url var="pagingURL" value="" scope="page">
                          <c:param name="page" value="${i}"></c:param>
                          <c:if test="${param.subjectId != null}">
                              <c:param name="subjectId" value="${param.subjectId}"></c:param>
                          </c:if>
                          <c:if test="${param.questionContent != null}">
                              <c:param name="questionContent" value="${param.questionContent}"></c:param>
                          </c:if>
                          <c:if test="${param.questionStatus != null}">
                              <c:param name="questionStatus" value="${param.questionStatus}"></c:param>
                          </c:if>
                          <c:if test="${param.subjectStatus != null}">
                              <c:param name="subjectStatus" value="${param.subjectStatus}"></c:param>
                          </c:if>
                      </c:url>
                      <a class="page-link" href="${pageScope.pagingURL}">${i}</a>
                    </li>
                </c:forEach>
              </ul>
            </nav>
      </c:if>

      <!--Main Content End--> 
    </main>
    <jsp:include page="/WEB-INF/section/footer.jsp"/>
    <jsp:include page="/WEB-INF/section/script.jsp"/>
  </body>

</html>
