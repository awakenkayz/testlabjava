<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="modalCreate" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="MainController" method="POST" class="text-left">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Create new question</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Question Content:
          <textarea style="resize: none" class="form-control mb-1" rows="3" name="questionContent" required></textarea>
          Answer1:
          <textarea style="resize: none" class="form-control mb-1" rows="2" name="answer1" required></textarea>
          Answer2:
          <textarea style="resize: none" class="form-control mb-1" rows="2" name="answer2" required></textarea>
          Answer3:
          <textarea style="resize: none" class="form-control mb-1" rows="2" name="answer3" required></textarea>
          Answer4:
          <textarea style="resize: none" class="form-control mb-1" rows="2" name="answer4" required></textarea>
          Correct Answer:
          <input type="number" class="form-control mb-1" name="answerCorrect" min="1" max="4" step="1" required>
          Subject:
          <select class="form-control" name="subjectId">
            <c:forEach items="${requestScope.SUBJECTS}" var="subject">
                <option value="${subject.subjectId}">${subject.name}</option>
            </c:forEach>
          </select>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" name="btnAction" value="createQuestion">Create</button>
        </div>
      </div>
    </form>
  </div>
</div>

