USE [master]
GO
/****** Object:  Database [QuizOnline]    Script Date: 20-Feb-21 3:22:27 PM ******/
CREATE DATABASE [QuizOnline]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuizOnline', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\QuizOnline.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QuizOnline_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\QuizOnline_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuizOnline].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuizOnline] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuizOnline] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuizOnline] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuizOnline] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuizOnline] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuizOnline] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuizOnline] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuizOnline] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuizOnline] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuizOnline] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuizOnline] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuizOnline] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuizOnline] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuizOnline] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuizOnline] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QuizOnline] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuizOnline] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuizOnline] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuizOnline] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuizOnline] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuizOnline] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuizOnline] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuizOnline] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QuizOnline] SET  MULTI_USER 
GO
ALTER DATABASE [QuizOnline] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuizOnline] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuizOnline] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuizOnline] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
USE [QuizOnline]
GO
/****** Object:  Table [dbo].[tblQuestions]    Script Date: 20-Feb-21 3:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblQuestions](
	[questionId] [int] IDENTITY(1,1) NOT NULL,
	[questionContent] [nvarchar](max) NULL,
	[answer1] [nvarchar](max) NULL,
	[answer2] [nvarchar](max) NULL,
	[answer3] [nvarchar](max) NULL,
	[answer4] [nvarchar](max) NULL,
	[answerCorrect] [int] NULL,
	[createdAt] [datetime] NULL,
	[subjectId] [varchar](10) NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_tblQuestions] PRIMARY KEY CLUSTERED 
(
	[questionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblQuizDetails]    Script Date: 20-Feb-21 3:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblQuizDetails](
	[quizDetailId] [int] IDENTITY(1,1) NOT NULL,
	[quizId] [varchar](50) NULL,
	[questionId] [int] NULL,
	[selectedAnswer] [int] NULL,
	[isCorrect] [bit] NULL,
 CONSTRAINT [PK_tblQuizDetails] PRIMARY KEY CLUSTERED 
(
	[quizDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblQuizs]    Script Date: 20-Feb-21 3:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblQuizs](
	[quizId] [varchar](50) NOT NULL,
	[subjectId] [varchar](10) NULL,
	[createdAt] [datetime] NULL,
	[result] [float] NULL,
	[userId] [int] NULL,
	[submitAt] [datetime] NULL,
 CONSTRAINT [PK_tblQuizs] PRIMARY KEY CLUSTERED 
(
	[quizId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblRoles]    Script Date: 20-Feb-21 3:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRoles](
	[roleId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblRoles] PRIMARY KEY CLUSTERED 
(
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblSubjects]    Script Date: 20-Feb-21 3:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSubjects](
	[subjectId] [varchar](10) NOT NULL,
	[name] [nvarchar](50) NULL,
	[numberOfQuestions] [int] NULL,
	[time] [int] NULL,
	[status] [int] NULL,
 CONSTRAINT [PK_tblSubjects] PRIMARY KEY CLUSTERED 
(
	[subjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tblUsers]    Script Date: 20-Feb-21 3:22:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUsers](
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[roleId] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varbinary](150) NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_tblUsers] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tblQuestions] ON 

INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (2, N'0+0=?', N'-22', N'0', N'2', N'9991', 2, CAST(N'2021-02-19T10:39:12.057' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (3, N'0+1=?', N'-2', N'-1', N'1', N'999', 3, CAST(N'2021-02-19T10:39:12.060' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (4, N'0+2=?', N'-2', N'-1', N'2', N'999', 3, CAST(N'2021-02-19T10:39:12.060' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (5, N'0+3=?', N'-2', N'-1', N'3', N'999', 3, CAST(N'2021-02-19T10:39:12.060' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (6, N'0+4=?', N'-2', N'-1', N'4', N'999', 3, CAST(N'2021-02-19T10:39:12.060' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (7, N'0+5=?', N'-2', N'-1', N'5', N'999', 3, CAST(N'2021-02-19T10:39:12.060' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (8, N'0+6=?', N'-2', N'-1', N'6', N'999', 3, CAST(N'2021-02-19T10:39:12.060' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (9, N'0+7=?', N'-2', N'-1', N'7', N'999', 3, CAST(N'2021-02-19T10:39:12.063' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (10, N'0+8=?', N'-2', N'-1', N'8', N'999', 3, CAST(N'2021-02-19T10:39:12.063' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (11, N'0+9=?', N'-2', N'-1', N'9', N'999', 3, CAST(N'2021-02-19T10:39:12.063' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (12, N'0+10=?', N'-2', N'-1', N'10', N'999', 3, CAST(N'2021-02-19T10:39:12.063' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (13, N'0+11=?', N'-2', N'-1', N'11', N'999', 3, CAST(N'2021-02-19T10:39:12.067' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (14, N'0+12=?', N'-2', N'-1', N'12', N'999', 3, CAST(N'2021-02-19T10:39:12.067' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (15, N'0+13=?', N'-2', N'-1', N'13', N'999', 3, CAST(N'2021-02-19T10:39:12.067' AS DateTime), N'PRJ321', 0)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (16, N'0+14=?', N'-2', N'-1', N'14', N'999', 3, CAST(N'2021-02-19T10:39:12.067' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (17, N'0+15=?', N'-2', N'-1', N'15', N'999', 3, CAST(N'2021-02-19T10:39:12.067' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (18, N'0+16=?', N'-2', N'-1', N'16', N'999', 3, CAST(N'2021-02-19T10:39:12.067' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (19, N'0+17=?', N'-2', N'-1', N'17', N'999', 3, CAST(N'2021-02-19T10:39:12.070' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (20, N'0+18=?', N'-2', N'-1', N'18', N'999', 3, CAST(N'2021-02-19T10:39:12.073' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (21, N'0+19=?', N'-2', N'-1', N'19', N'999', 3, CAST(N'2021-02-19T10:39:12.073' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (22, N'0+20=?', N'-2', N'-1', N'20', N'999', 3, CAST(N'2021-02-19T10:39:12.073' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (23, N'0+21=?', N'-2', N'-1', N'21', N'999', 3, CAST(N'2021-02-19T10:39:12.073' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (24, N'0+22=?', N'-2', N'-1', N'22', N'999', 3, CAST(N'2021-02-19T10:39:12.073' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (25, N'0+23=?', N'-2', N'-1', N'23', N'999', 3, CAST(N'2021-02-19T10:39:12.073' AS DateTime), N'PRJ321', 0)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (26, N'0+24=?', N'-2', N'-1', N'24', N'999', 3, CAST(N'2021-02-19T10:39:12.073' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (27, N'0+25=?', N'-2', N'-1', N'25', N'999', 3, CAST(N'2021-02-19T10:39:12.077' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (28, N'0+26=?', N'-2', N'-1', N'26', N'999', 3, CAST(N'2021-02-19T10:39:12.077' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (29, N'0+27=?', N'-2', N'-1', N'27', N'999', 3, CAST(N'2021-02-19T10:39:12.077' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (30, N'0+28=?', N'-2', N'-1', N'28', N'999', 3, CAST(N'2021-02-19T10:39:12.077' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (31, N'0+29=?', N'-2', N'-1', N'29', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (32, N'0+30=?', N'-2', N'-1', N'30', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (33, N'0+31=?', N'-2', N'-1', N'31', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (34, N'0+32=?', N'-2', N'-1', N'32', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (35, N'0+33=?', N'-2', N'-1', N'33', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (36, N'0+34=?', N'-2', N'-1', N'34', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (37, N'0+35=?', N'-2', N'-1', N'35', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (38, N'0+36=?', N'-2', N'-1', N'36', N'999', 3, CAST(N'2021-02-19T10:39:12.080' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (39, N'0+37=?', N'-2', N'-1', N'37', N'999', 3, CAST(N'2021-02-19T10:39:12.083' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (40, N'0+38=?', N'-2', N'-1', N'38', N'999', 3, CAST(N'2021-02-19T10:39:12.083' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (41, N'0+39=?', N'-2', N'-1', N'39', N'999', 3, CAST(N'2021-02-19T10:39:12.083' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (42, N'0+40=?', N'-2', N'-1', N'40', N'999', 3, CAST(N'2021-02-19T10:39:12.083' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (43, N'0+41=?', N'-2', N'-1', N'41', N'999', 3, CAST(N'2021-02-19T10:39:12.083' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (44, N'0+42=?', N'-2', N'-1', N'42', N'999', 3, CAST(N'2021-02-19T10:39:12.087' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (45, N'0+43=?', N'-2', N'-1', N'43', N'999', 3, CAST(N'2021-02-19T10:39:12.087' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (46, N'0+44=?', N'-2', N'-1', N'44', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (47, N'0+45=?', N'-2', N'-1', N'45', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (48, N'0+46=?', N'-2', N'-1', N'46', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (49, N'0+47=?', N'-2', N'-1', N'47', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (50, N'0+48=?', N'-2', N'-1', N'48', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (51, N'0+49=?', N'-2', N'-1', N'49', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (52, N'0+50=?', N'-2', N'-1', N'50', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (53, N'0+51=?', N'-2', N'-1', N'51', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (54, N'0+52=?', N'-2', N'-1', N'52', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (55, N'0+53=?', N'-2', N'-1', N'53', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (56, N'0+54=?', N'-2', N'-1', N'54', N'999', 3, CAST(N'2021-02-19T10:39:12.090' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (57, N'0+55=?', N'-2', N'-1', N'55', N'999', 3, CAST(N'2021-02-19T10:39:12.093' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (58, N'0+56=?', N'-2', N'-1', N'56', N'999', 3, CAST(N'2021-02-19T10:39:12.093' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (59, N'0+57=?', N'-2', N'-1', N'57', N'999', 3, CAST(N'2021-02-19T10:39:12.093' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (60, N'0+58=?', N'-2', N'-1', N'58', N'999', 3, CAST(N'2021-02-19T10:39:12.093' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (61, N'0+59=?', N'-2', N'-1', N'59', N'999', 3, CAST(N'2021-02-19T10:39:12.093' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (62, N'0+60=?', N'-2', N'-1', N'60', N'999', 3, CAST(N'2021-02-19T10:39:12.093' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (63, N'0+61=?', N'-2', N'-1', N'61', N'999', 3, CAST(N'2021-02-19T10:39:12.093' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (64, N'0+62=?', N'-2', N'-1', N'62', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (65, N'0+63=?', N'-2', N'-1', N'63', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (66, N'0+64=?', N'-2', N'-1', N'64', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (67, N'0+65=?', N'-2', N'-1', N'65', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (68, N'0+66=?', N'-2', N'-1', N'66', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (69, N'0+67=?', N'-2', N'-1', N'67', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (70, N'0+68=?', N'-2', N'-1', N'68', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (71, N'0+69=?', N'-2', N'-1', N'69', N'999', 3, CAST(N'2021-02-19T10:39:12.097' AS DateTime), N'SWR201', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (72, N'0+300=?', N'-2', N'-1', N'300', N'999', 3, CAST(N'2021-02-19T13:35:49.603' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (73, N'0+301=?', N'-2', N'-1', N'301', N'999', 3, CAST(N'2021-02-19T14:42:27.253' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (74, N'0+302=?', N'-2', N'302', N'-1', N'999', 2, CAST(N'2021-02-19T17:22:11.883' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (75, N'0+70', N'-1', N'-2', N'70', N'999', 3, CAST(N'2021-02-20T10:29:32.643' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (76, N'1+1=?', N'-1', N'2', N'3', N'4', 2, CAST(N'2021-02-20T11:52:28.787' AS DateTime), N'PRJ321', 1)
INSERT [dbo].[tblQuestions] ([questionId], [questionContent], [answer1], [answer2], [answer3], [answer4], [answerCorrect], [createdAt], [subjectId], [status]) VALUES (77, N'0+0+0=?', N'0', N'1', N'2', N'3', 1, CAST(N'2021-02-20T11:57:48.917' AS DateTime), N'PRJ321', 1)
SET IDENTITY_INSERT [dbo].[tblQuestions] OFF
GO
SET IDENTITY_INSERT [dbo].[tblQuizDetails] ON 

INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (240, N'7845f090-4ff3-4568-b593-318157f9a658', 9, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (241, N'7845f090-4ff3-4568-b593-318157f9a658', 39, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (242, N'7845f090-4ff3-4568-b593-318157f9a658', 8, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (243, N'7845f090-4ff3-4568-b593-318157f9a658', 33, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (244, N'7845f090-4ff3-4568-b593-318157f9a658', 35, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (245, N'7845f090-4ff3-4568-b593-318157f9a658', 14, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (246, N'7845f090-4ff3-4568-b593-318157f9a658', 2, 2, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (247, N'7845f090-4ff3-4568-b593-318157f9a658', 30, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (248, N'7845f090-4ff3-4568-b593-318157f9a658', 3, 2, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (249, N'7845f090-4ff3-4568-b593-318157f9a658', 24, 4, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (250, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 60, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (251, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 61, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (252, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 58, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (253, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 51, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (254, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 62, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (255, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 65, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (256, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 68, 4, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (257, N'8c46a089-e04c-461b-abda-e0e271a91b9e', 54, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (258, N'fc69233f-b1bc-4543-b21a-607e194093e1', 59, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (259, N'fc69233f-b1bc-4543-b21a-607e194093e1', 70, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (260, N'fc69233f-b1bc-4543-b21a-607e194093e1', 50, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (261, N'fc69233f-b1bc-4543-b21a-607e194093e1', 68, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (262, N'fc69233f-b1bc-4543-b21a-607e194093e1', 43, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (263, N'fc69233f-b1bc-4543-b21a-607e194093e1', 69, -1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (264, N'fc69233f-b1bc-4543-b21a-607e194093e1', 51, -1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (265, N'fc69233f-b1bc-4543-b21a-607e194093e1', 42, -1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (266, N'99f0f157-4195-409f-92c0-66c731165e3b', 5, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (267, N'99f0f157-4195-409f-92c0-66c731165e3b', 40, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (268, N'99f0f157-4195-409f-92c0-66c731165e3b', 31, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (269, N'99f0f157-4195-409f-92c0-66c731165e3b', 13, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (270, N'99f0f157-4195-409f-92c0-66c731165e3b', 7, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (271, N'99f0f157-4195-409f-92c0-66c731165e3b', 73, 4, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (272, N'99f0f157-4195-409f-92c0-66c731165e3b', 3, 2, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (273, N'99f0f157-4195-409f-92c0-66c731165e3b', 12, 1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (274, N'99f0f157-4195-409f-92c0-66c731165e3b', 6, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (275, N'99f0f157-4195-409f-92c0-66c731165e3b', 18, 2, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (276, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 64, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (277, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 57, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (278, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 50, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (279, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 61, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (280, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 63, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (281, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 71, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (282, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 66, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (283, N'ed5deca8-3cbb-4598-8723-0837f045c6a3', 48, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (284, N'd692773f-9516-4fe6-9d63-3de16170ef68', 40, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (285, N'd692773f-9516-4fe6-9d63-3de16170ef68', 30, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (286, N'd692773f-9516-4fe6-9d63-3de16170ef68', 18, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (287, N'd692773f-9516-4fe6-9d63-3de16170ef68', 17, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (288, N'd692773f-9516-4fe6-9d63-3de16170ef68', 37, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (289, N'd692773f-9516-4fe6-9d63-3de16170ef68', 4, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (290, N'd692773f-9516-4fe6-9d63-3de16170ef68', 75, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (291, N'd692773f-9516-4fe6-9d63-3de16170ef68', 74, -1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (292, N'd692773f-9516-4fe6-9d63-3de16170ef68', 16, -1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (293, N'd692773f-9516-4fe6-9d63-3de16170ef68', 5, -1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (294, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 45, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (295, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 43, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (296, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 65, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (297, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 57, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (298, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 48, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (299, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 69, 3, 1)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (300, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 66, -1, 0)
INSERT [dbo].[tblQuizDetails] ([quizDetailId], [quizId], [questionId], [selectedAnswer], [isCorrect]) VALUES (301, N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', 60, -1, 0)
SET IDENTITY_INSERT [dbo].[tblQuizDetails] OFF
GO
INSERT [dbo].[tblQuizs] ([quizId], [subjectId], [createdAt], [result], [userId], [submitAt]) VALUES (N'2d4a332d-d9dd-4a9a-8c0c-f0db3bb8a0ff', N'SWR201', CAST(N'2021-02-20T12:02:26.617' AS DateTime), 7.5, 9, CAST(N'2021-02-20T12:02:41.827' AS DateTime))
INSERT [dbo].[tblQuizs] ([quizId], [subjectId], [createdAt], [result], [userId], [submitAt]) VALUES (N'7845f090-4ff3-4568-b593-318157f9a658', N'PRJ321', CAST(N'2021-02-20T10:33:00.910' AS DateTime), 8, 9, CAST(N'2021-02-20T10:33:57.900' AS DateTime))
INSERT [dbo].[tblQuizs] ([quizId], [subjectId], [createdAt], [result], [userId], [submitAt]) VALUES (N'8c46a089-e04c-461b-abda-e0e271a91b9e', N'SWR201', CAST(N'2021-02-20T10:34:44.260' AS DateTime), 8.6999998092651367, 9, CAST(N'2021-02-20T10:35:17.640' AS DateTime))
INSERT [dbo].[tblQuizs] ([quizId], [subjectId], [createdAt], [result], [userId], [submitAt]) VALUES (N'99f0f157-4195-409f-92c0-66c731165e3b', N'PRJ321', CAST(N'2021-02-20T11:50:17.947' AS DateTime), 6, 12, CAST(N'2021-02-20T11:50:32.450' AS DateTime))
INSERT [dbo].[tblQuizs] ([quizId], [subjectId], [createdAt], [result], [userId], [submitAt]) VALUES (N'd692773f-9516-4fe6-9d63-3de16170ef68', N'PRJ321', CAST(N'2021-02-20T11:58:37.727' AS DateTime), 7, 12, CAST(N'2021-02-20T11:58:52.573' AS DateTime))
INSERT [dbo].[tblQuizs] ([quizId], [subjectId], [createdAt], [result], [userId], [submitAt]) VALUES (N'ed5deca8-3cbb-4598-8723-0837f045c6a3', N'SWR201', CAST(N'2021-02-20T11:50:47.660' AS DateTime), 10, 12, CAST(N'2021-02-20T11:50:57.403' AS DateTime))
INSERT [dbo].[tblQuizs] ([quizId], [subjectId], [createdAt], [result], [userId], [submitAt]) VALUES (N'fc69233f-b1bc-4543-b21a-607e194093e1', N'SWR201', CAST(N'2021-02-20T10:36:37.340' AS DateTime), 6.1999998092651367, 9, CAST(N'2021-02-20T10:38:38.927' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tblRoles] ON 

INSERT [dbo].[tblRoles] ([roleId], [name]) VALUES (1, N'ad')
INSERT [dbo].[tblRoles] ([roleId], [name]) VALUES (2, N'us')
SET IDENTITY_INSERT [dbo].[tblRoles] OFF
GO
INSERT [dbo].[tblSubjects] ([subjectId], [name], [numberOfQuestions], [time], [status]) VALUES (N'PRJ321', N'Java Web', 10, 5, 1)
INSERT [dbo].[tblSubjects] ([subjectId], [name], [numberOfQuestions], [time], [status]) VALUES (N'SWR201', N'Software Requirement', 8, 2, 1)
GO
SET IDENTITY_INSERT [dbo].[tblUsers] ON 

INSERT [dbo].[tblUsers] ([userId], [roleId], [name], [email], [password], [status]) VALUES (8, 1, N'admin', N'admin@gmail.com', 0xE79E418E48623569D75E2A7B09AE88ED9B77B126A445B9FF9DC6989A08EFA079, 1)
INSERT [dbo].[tblUsers] ([userId], [roleId], [name], [email], [password], [status]) VALUES (9, 2, N'user', N'user@gmail.com', 0xE79E418E48623569D75E2A7B09AE88ED9B77B126A445B9FF9DC6989A08EFA079, 1)
INSERT [dbo].[tblUsers] ([userId], [roleId], [name], [email], [password], [status]) VALUES (12, 2, N'thucpn', N'thucsh2@gmail.com', 0xE79E418E48623569D75E2A7B09AE88ED9B77B126A445B9FF9DC6989A08EFA079, 1)
SET IDENTITY_INSERT [dbo].[tblUsers] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_tblUsers]    Script Date: 20-Feb-21 3:22:27 PM ******/
ALTER TABLE [dbo].[tblUsers] ADD  CONSTRAINT [IX_tblUsers] UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblQuestions] ADD  CONSTRAINT [DF_tblQuestions_status]  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[tblSubjects] ADD  CONSTRAINT [DF_tblSubjects_status]  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[tblUsers] ADD  CONSTRAINT [DF_tblUsers_status]  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[tblQuestions]  WITH CHECK ADD  CONSTRAINT [FK_tblQuestions_tblSubjects] FOREIGN KEY([subjectId])
REFERENCES [dbo].[tblSubjects] ([subjectId])
GO
ALTER TABLE [dbo].[tblQuestions] CHECK CONSTRAINT [FK_tblQuestions_tblSubjects]
GO
ALTER TABLE [dbo].[tblQuizDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblQuizDetails_tblQuestions] FOREIGN KEY([questionId])
REFERENCES [dbo].[tblQuestions] ([questionId])
GO
ALTER TABLE [dbo].[tblQuizDetails] CHECK CONSTRAINT [FK_tblQuizDetails_tblQuestions]
GO
ALTER TABLE [dbo].[tblQuizDetails]  WITH CHECK ADD  CONSTRAINT [FK_tblQuizDetails_tblQuizs] FOREIGN KEY([quizId])
REFERENCES [dbo].[tblQuizs] ([quizId])
GO
ALTER TABLE [dbo].[tblQuizDetails] CHECK CONSTRAINT [FK_tblQuizDetails_tblQuizs]
GO
ALTER TABLE [dbo].[tblQuizs]  WITH CHECK ADD  CONSTRAINT [FK_tblQuizs_tblSubjects] FOREIGN KEY([subjectId])
REFERENCES [dbo].[tblSubjects] ([subjectId])
GO
ALTER TABLE [dbo].[tblQuizs] CHECK CONSTRAINT [FK_tblQuizs_tblSubjects]
GO
ALTER TABLE [dbo].[tblQuizs]  WITH CHECK ADD  CONSTRAINT [FK_tblQuizs_tblUsers] FOREIGN KEY([userId])
REFERENCES [dbo].[tblUsers] ([userId])
GO
ALTER TABLE [dbo].[tblQuizs] CHECK CONSTRAINT [FK_tblQuizs_tblUsers]
GO
ALTER TABLE [dbo].[tblUsers]  WITH CHECK ADD  CONSTRAINT [FK_tblUsers_tblRoles] FOREIGN KEY([roleId])
REFERENCES [dbo].[tblRoles] ([roleId])
GO
ALTER TABLE [dbo].[tblUsers] CHECK CONSTRAINT [FK_tblUsers_tblRoles]
GO
USE [master]
GO
ALTER DATABASE [QuizOnline] SET  READ_WRITE 
GO
