package thucpn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Vector;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import thucpn.daos.SubjectDAO;
import thucpn.daos.QuestionDAO;
import thucpn.dtos.SubjectDTO;
import thucpn.dtos.QuestionDTO;

@WebServlet(name = "ManageQuestionController", urlPatterns = {"/ManageQuestionController"})
public class ManageQuestionController extends HttpServlet {

    private final Logger LOG = Logger.getLogger(ManageQuestionController.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = "manage.jsp";

        try {
            int page;
            String subjectId;
            String questionContent;
            int questionStatus;
            int subjectStatus;

            String pageString = request.getParameter("page");
            if (pageString == null) {
                page = 1;
            } else {
                page = Integer.parseInt(pageString);
            }

            subjectId = request.getParameter("subjectId");
            if (subjectId == null) {
                subjectId = "PRJ321";
            }

            questionContent = request.getParameter("questionContent");
            if (questionContent == null) {
                questionContent = "";
            }

            String questionStatusString = request.getParameter("questionStatus");
            if (questionStatusString == null) {
                questionStatus = -1;
            } else {
                questionStatus = Integer.parseInt(questionStatusString);
            }

            String subjectStatusString = request.getParameter("subjectStatus");
            if (subjectStatusString == null) {
                subjectStatus = -1;
            } else {
                subjectStatus = Integer.parseInt(subjectStatusString);
            }

            Vector<QuestionDTO> questions = QuestionDAO.getQuestions(page, subjectId, questionContent, questionStatus, subjectStatus);
            Vector<SubjectDTO> subjects = SubjectDAO.getSubjects();
            int numberOfQuestions = QuestionDAO.getNumberOfQuestions(subjectId, questionContent, questionStatus, subjectStatus);

            request.setAttribute("QUESTIONS", questions);
            request.setAttribute("SUBJECTS", subjects);
            request.setAttribute("NUMBER_OF_QUESTIONS", numberOfQuestions);

        } catch (ClassNotFoundException | NumberFormatException | SQLException | NamingException e) {
            LOG.error(e.toString());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
