package thucpn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import thucpn.daos.UserDAO;
import thucpn.dtos.UserDTO;

@WebServlet(name = "RegisterController", urlPatterns = {"/RegisterController"})
public class RegisterController extends HttpServlet {

    private final Logger LOG = Logger.getLogger(RegisterController.class);

    private static final String USER = "/QuizOnline";
    private static final String ADMIN = "ManageQuestionController";
    private static final String ERROR = "register.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;

        try {
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String password = request.getParameter("password");

            if (name == null || email == null || password == null) {
                request.setAttribute("ERROR", "Name, Email, Password not empty");
            } else if (UserDAO.checkEmailExisted(email)) {
                request.setAttribute("ERROR", "Email existed");
            } else {
                boolean result = UserDAO.register(name, email, password);

                if (result) {
                    UserDTO userDTO = UserDAO.getUserByEmail(email);
                    HttpSession session = request.getSession();
                    session.setAttribute("CUR_USER", userDTO);
                    if (userDTO.getRoleName().equals("us")) {
                        url = USER;
                    }
                    if (userDTO.getRoleName().equals("ad")) {
                        url = ADMIN;
                    }
                } else {
                    request.setAttribute("ERROR", "Register fail!");
                }
            }
        } catch (ClassNotFoundException | NumberFormatException | SQLException | NamingException e) {
            request.setAttribute("ERROR", "Something wrong!");
            LOG.error(e.toString());
        } finally {
            if (url.equals(ERROR)) {
                request.getRequestDispatcher(url).forward(request, response);
            } else {
                response.sendRedirect(url);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
