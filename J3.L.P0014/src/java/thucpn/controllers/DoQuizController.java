package thucpn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;
import java.util.Vector;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import thucpn.daos.QuestionDAO;
import thucpn.daos.QuizDAO;
import thucpn.dtos.QuestionDTO;
import thucpn.dtos.UserDTO;

@WebServlet(name = "DoQuizController", urlPatterns = {"/DoQuizController"})
public class DoQuizController extends HttpServlet {

    private final Logger LOG = Logger.getLogger(DoQuizController.class);
    private final String FAIL = "404.jsp";
    private final String SUCCESS = "quiz.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = FAIL;

        try {
            HttpSession session = request.getSession();
            UserDTO userDTO = (UserDTO) session.getAttribute("CUR_USER");

            int userId = userDTO.getUserId();
            String subjectId = request.getParameter("subjectId");
            String name = request.getParameter("name");
            int numberOfQuestions = Integer.parseInt(request.getParameter("numberOfQuestions"));

            // create quiz with default value
            String quizId = UUID.randomUUID().toString();
            QuizDAO.createQuiz(quizId, subjectId, userId);

            // get random n question
            Vector<QuestionDTO> questions = QuestionDAO.getQuizQuestions(subjectId, name, numberOfQuestions);

            request.setAttribute("QUIZ_ID", quizId);
            request.setAttribute("QUESTIONS", questions);

            url = SUCCESS;
        } catch (NumberFormatException | SQLException | NamingException | ClassNotFoundException e) {
            LOG.error(e.toString());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
