package thucpn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Vector;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import thucpn.daos.UserDAO;
import thucpn.dtos.HistoryDTO;
import thucpn.dtos.UserDTO;

@WebServlet(name = "ViewHistoryController", urlPatterns = {"/ViewHistoryController"})
public class ViewHistoryController extends HttpServlet {

    private final Logger LOG = Logger.getLogger(ViewHistoryController.class);
    private static final String ERROR = "500.jsp";
    private static final String SUCCESS = "history.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;

        try {
            int page;
            String pageString = request.getParameter("page");
            if (pageString == null) {
                page = 1;
            } else {
                page = Integer.parseInt(pageString);
            }
            String name = request.getParameter("name");
            if (name == null) {
                name = "";
            }

            HttpSession session = request.getSession();
            UserDTO curUser = (UserDTO) session.getAttribute("CUR_USER");

            Vector<HistoryDTO> historys = UserDAO.getHistory(page, curUser.getUserId(), name);
            int numberOfHistoryLine = UserDAO.getNumberOfHistoryLine(curUser.getUserId(), name);

            request.setAttribute("HISTORYS", historys);
            request.setAttribute("NUMBER_OF_HISTORYLINES", numberOfHistoryLine);

            url = SUCCESS;
        } catch (ClassNotFoundException | SQLException | NamingException e) {
            LOG.error(e.toString());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
