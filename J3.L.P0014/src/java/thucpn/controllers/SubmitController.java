package thucpn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Vector;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import thucpn.daos.QuizDAO;
import thucpn.dtos.QuizDTO;
import thucpn.dtos.QuizDetailDTO;

@WebServlet(name = "SubmitController", urlPatterns = {"/SubmitController"})
public class SubmitController extends HttpServlet {

    private final Logger LOG = Logger.getLogger(SubmitController.class);

    private static final String SUCCESS = "result.jsp";
    private static final String ERROR = "404.jsp";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;

        try {
            String quizId = request.getParameter("quizId");
            int numberOfQuestions = Integer.parseInt(request.getParameter("numberOfQuestions"));

            Vector<QuizDetailDTO> quizDetails = new Vector<>();

            int count = 0;
            for (int i = 0; i < numberOfQuestions; i++) {
                String questionIdString = request.getParameter("question" + i);
                String answerCorrectString = request.getParameter("answerCorrect" + i);
                String selectedAnswerString = request.getParameter("answerForQuestion" + i);

                int questionId = Integer.parseInt(questionIdString);
                int answerCorrect = Integer.parseInt(answerCorrectString);
                int selectedAnswer = selectedAnswerString != null ? Integer.parseInt(selectedAnswerString) : -1;
                boolean isCorrect = answerCorrect == selectedAnswer;

                quizDetails.add(new QuizDetailDTO(-1, quizId, questionId, selectedAnswer, isCorrect));

                if (isCorrect) {
                    count++;
                }
            }

            float result = Math.round(count * 100 / numberOfQuestions) / 10.0f;
            QuizDTO quiz = new QuizDTO(quizId, null, null, result, null, -1);

            boolean res = QuizDAO.submit(quiz, quizDetails);
            if (res) {
                request.setAttribute("NUMBER_OF_CORRECT", count);
                request.setAttribute("NUMBER_OF_QUESTIONS", numberOfQuestions);
                request.setAttribute("POINT", result);
                url = SUCCESS;
            }
        } catch (ClassNotFoundException | NumberFormatException | SQLException | NamingException e) {
            LOG.error(e.toString());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
