package thucpn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import thucpn.daos.QuestionDAO;
import thucpn.dtos.UserDTO;

@WebServlet(name = "CreateQuestionController", urlPatterns = {"/CreateQuestionController"})
public class CreateQuestionController extends HttpServlet {

    private final Logger LOG = Logger.getLogger(CreateQuestionController.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = "ManageQuestionController";

        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("CUR_USER") != null) {
                UserDTO userDTO = (UserDTO) session.getAttribute("CUR_USER");
                int roleId = userDTO.getRoleId();
                if (roleId == 1) {
                    String questionContent = request.getParameter("questionContent");
                    String answer1 = request.getParameter("answer1");
                    String answer2 = request.getParameter("answer2");
                    String answer3 = request.getParameter("answer3");
                    String answer4 = request.getParameter("answer4");
                    int answerCorrect = Integer.parseInt(request.getParameter("answerCorrect"));
                    String subjectId = request.getParameter("subjectId");

                    QuestionDAO.createQuestion(questionContent, answer1, answer2, answer3, answer4, answerCorrect, subjectId);
                }
            }

        } catch (ClassNotFoundException | NumberFormatException | SQLException | NamingException e) {
            LOG.error(e.toString());
        } finally {
            response.sendRedirect(url);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
