package thucpn.controllers;

import java.io.IOException;
import java.sql.SQLException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import thucpn.daos.QuestionDAO;
import thucpn.dtos.UserDTO;

@WebServlet(name = "DeleteQuestionController", urlPatterns = {"/DeleteQuestionController"})
public class DeleteQuestionController extends HttpServlet {

    private final Logger LOG = Logger.getLogger(DeleteQuestionController.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = "ManageQuestionController";

        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("CUR_USER") != null) {
                UserDTO curUser = (UserDTO) session.getAttribute("CUR_USER");
                int roleId = curUser.getRoleId();
                if (roleId == 1) {
                    String[] questionIds = request.getParameterValues("select");

                    if (questionIds != null && questionIds.length > 0) {
                        for (String questionId : questionIds) {
                            int id = Integer.parseInt(questionId);
                            QuestionDAO.deleteQuestion(id);
                        }
                    }
                }
            }

        } catch (ClassNotFoundException | NumberFormatException | SQLException | NamingException e) {
            LOG.error(e.toString());
        } finally {
            response.sendRedirect(url);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
