package thucpn.utilities;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class DatabaseHelper {

    public static Connection makeConnection() throws NamingException, SQLException {
        Context currentContext = new InitialContext();
        Context tomcatContext = (Context) currentContext.lookup("java:comp/env");
        DataSource dataSource = (DataSource) tomcatContext.lookup("DBConfig");
        Connection conn = dataSource.getConnection();
        return conn;
    }
}
