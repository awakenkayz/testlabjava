package thucpn.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import thucpn.dtos.UserDTO;

@WebFilter(filterName = "AuthenFilter", urlPatterns = {"/*"})
public class AuthenFilter implements Filter {

    private final ArrayList<String> userPages;
    private final ArrayList<String> adminPages;
    private final Map<String, String> statefulPages;

    public AuthenFilter() {
        // only user page
        userPages = new ArrayList<>();
        userPages.add("history.jsp");
        userPages.add("home.jsp");
        userPages.add("quiz.jsp");
        userPages.add("result.jsp");
        userPages.add("DoQuizController");
        userPages.add("SubmitController");
        userPages.add("ViewHistoryController");
        userPages.add("ViewSubjectController");

        // only admin page
        adminPages = new ArrayList<>();
        adminPages.add("manage.jsp");
        adminPages.add("CreateQuestionController");
        adminPages.add("DeleteQuestionController");
        adminPages.add("ManageQuestionController");
        adminPages.add("UpdateQuestionController");

        // statefulPages
        statefulPages = new HashMap<>();
        statefulPages.put("history.jsp", "/QuizOnline/ViewHistoryController");
        statefulPages.put("home.jsp", "/QuizOnline/ViewSubjectController");
        statefulPages.put("manage.jsp", "/QuizOnline/ManageQuestionController");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest rq = (HttpServletRequest) request;
        HttpServletResponse rs = (HttpServletResponse) response;
        HttpSession session = rq.getSession();

        String uri = rq.getRequestURI();

        // filter "/" page
        if (uri.equals("/QuizOnline/")) {
            rs.sendRedirect("ViewSubjectController");
            return;
        }

        // filter stateful jsp page
        for (Map.Entry<String, String> entry : statefulPages.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (uri.contains(key)) {
                rs.sendRedirect(value);
                return;
            }
        }

        // no filter for resoure uri
        if (uri.lastIndexOf(".js") == uri.length() - 3
                || uri.lastIndexOf(".css") == uri.length() - 4
                || uri.lastIndexOf(".png") == uri.length() - 4
                || uri.lastIndexOf(".jpg") == uri.length() - 4
                || uri.lastIndexOf(".jpeg") == uri.length() - 5) {
            chain.doFilter(request, response);
            return;
        }

        // no filter for public page
        if (uri.contains("404.jsp")
                || uri.contains("500.jsp")
                || (uri.contains("login.jsp") && session.getAttribute("CUR_USER") == null)
                || (uri.contains("register.jsp") && session.getAttribute("CUR_USER") == null)
                || uri.contains("MainController")
                || (uri.contains("LoginController") && session.getAttribute("CUR_USER") == null)
                || (uri.contains("RegisterController") && session.getAttribute("CUR_USER") == null)) {
            chain.doFilter(request, response);
            return;
        }

        // filter guest
        if (session.getAttribute("CUR_USER") == null) {
            rs.sendRedirect("/QuizOnline/login.jsp");
            return;
        }

        // filter already logged in
        if (uri.contains("login.jsp")
                || uri.contains("register.jsp")
                || uri.contains("LoginController")
                || uri.contains("RegisterController")) {
            rs.sendRedirect("/QuizOnline");
            return;
        }

        // filter common page between user and admin
        if (uri.contains("LogoutController")) {
            chain.doFilter(request, response);
            return;
        }

        UserDTO userDTO = (UserDTO) session.getAttribute("CUR_USER");
        int roleId = userDTO.getRoleId();

        // filter user access admin's page
        if (roleId == 2) {
            for (String adminPage : adminPages) {
                if (uri.contains(adminPage)) {
                    rs.sendRedirect("/QuizOnline/ViewSubjectController");
                    return;
                }
            }
        }

        // filter admin access user's page
        if (roleId == 1) {
            for (String userPage : userPages) {
                if (uri.contains(userPage)) {
                    rs.sendRedirect("/QuizOnline/ManageQuestionController");
                    return;
                }
            }
        }

        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }
}
