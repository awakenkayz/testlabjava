package thucpn.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName = "DispatcherFilter", urlPatterns = {"/MainController"})
public class DispatcherFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest rq = (HttpServletRequest) request;
        HttpServletResponse rs = (HttpServletResponse) response;

        String uri = rq.getRequestURI();
        String btnAction = rq.getParameter("btnAction");

        if (!uri.contains("MainController") || btnAction == null) {
            chain.doFilter(request, response);
            return;
        }

        switch (btnAction) {
            case "login":
                rq.getRequestDispatcher("LoginController").forward(rq, rs);
                return;
            case "register":
                rq.getRequestDispatcher("RegisterController").forward(rq, rs);
                return;
            case "logout":
                rq.getRequestDispatcher("LogoutController").forward(rq, rs);
                return;
            case "createQuestion":
                rq.getRequestDispatcher("CreateQuestionController").forward(rq, rs);
                return;
            case "updateQuestion":
                rq.getRequestDispatcher("UpdateQuestionController").forward(rq, rs);
                return;
            case "deleteQuestion":
                rq.getRequestDispatcher("DeleteQuestionController").forward(rq, rs);
                return;
            case "submit":
                rq.getRequestDispatcher("SubmitController").forward(rq, rs);
                return;
            case "viewHistory":
                rq.getRequestDispatcher("ViewHistoryController").forward(rq, rs);
                return;
            default:
                rs.sendRedirect("404.jsp");
        }
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }
}
