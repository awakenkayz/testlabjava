package thucpn.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;
import javax.naming.NamingException;
import thucpn.dtos.QuestionDTO;
import thucpn.utilities.DatabaseHelper;

public class QuestionDAO implements Serializable {

    public static Vector<QuestionDTO> getQuestions(int page, String searchSubjectId, String searchQuestionContent, int searchQuestionStatus, int searchSubjectStatus) throws ClassNotFoundException, SQLException, NamingException {
        Vector<QuestionDTO> result = new Vector<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql
                    = "SELECT questionId, questionContent, answer1, answer2, answer3, answer4, answerCorrect, createdAt, subjectId, subjectName, questionStatus, subjectStatus\n"
                    + "FROM \n"
                    + "(\n"
                    + "SELECT \n"
                    + "questionId, questionContent, answer1, answer2, answer3, answer4, answerCorrect, createdAt, tblSubjects.subjectId, tblSubjects.name as subjectName, tblQuestions.status as questionStatus, tblSubjects.status as subjectStatus,\n"
                    + "ROW_NUMBER() OVER (ORDER BY questionContent) as row \n"
                    + "FROM tblQuestions, tblSubjects\n"
                    + "WHERE\n"
                    + "tblQuestions.subjectId = tblSubjects.subjectId and\n"
                    + "tblSubjects.subjectId = ? and\n"
                    + "questionContent like ?\n"
                    + (searchQuestionStatus == -1 ? "and -1 = ?\n" : "and tblQuestions.status = ?\n")
                    + (searchSubjectStatus == -1 ? "and -1 = ?\n" : "and tblSubjects.status = ?\n")
                    + ")\n"
                    + "a \n"
                    + "WHERE row >= ? and row <= ?";

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, searchSubjectId);
            preparedStatement.setString(2, "%" + searchQuestionContent + "%");
            preparedStatement.setInt(3, searchQuestionStatus);
            preparedStatement.setInt(4, searchSubjectStatus);
            preparedStatement.setInt(5, (page - 1) * 20 + 1);
            preparedStatement.setInt(6, page * 20);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int questionId = resultSet.getInt(1);
                String questionContent = resultSet.getString(2);
                String answer1 = resultSet.getString(3);
                String answer2 = resultSet.getString(4);
                String answer3 = resultSet.getString(5);
                String answer4 = resultSet.getString(6);
                int answerCorrect = resultSet.getInt(7);
                Date createdAt = resultSet.getDate(8);
                String subjectId = resultSet.getString(9);
                String subjectName = resultSet.getString(10);
                int questionStatus = resultSet.getInt(11);
                int subjectStatus = resultSet.getInt(12);

                QuestionDTO dto = new QuestionDTO(questionId, questionContent, answer1, answer2, answer3, answer4, answerCorrect, createdAt, subjectId, subjectName, questionStatus, subjectStatus);
                result.add(dto);
            }
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static int getNumberOfQuestions(String searchSubjectId, String searchQuestionContent, int searchQuestionStatus, int searchSubjectStatus) throws SQLException, NamingException {
        int result = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql
                    = "SELECT \n"
                    + "count(questionId)\n"
                    + "FROM tblQuestions, tblSubjects\n"
                    + "WHERE \n"
                    + "tblQuestions.subjectId = tblSubjects.subjectId and\n"
                    + "tblSubjects.subjectId = ? and\n"
                    + "questionContent like ?\n"
                    + (searchQuestionStatus == -1 ? "and -1 = ?\n" : "and tblQuestions.status = ?\n")
                    + (searchSubjectStatus == -1 ? "and -1 = ?\n" : "and tblSubjects.status = ?\n");

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, searchSubjectId);
            preparedStatement.setString(2, "%" + searchQuestionContent + "%");
            preparedStatement.setInt(3, searchQuestionStatus);
            preparedStatement.setInt(4, searchSubjectStatus);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static boolean createQuestion(String questionContent, String answer1, String answer2, String answer3, String answer4, int answerCorrect, String subjectId) throws ClassNotFoundException, SQLException, NamingException {
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            String sql = "insert into \n"
                    + "tblQuestions(questionContent, answer1, answer2, answer3, answer4, answerCorrect, createdAt, subjectId)\n"
                    + "values(?, ?, ?, ?, ?, ?, GETDATE(), ?)";
            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, questionContent);
            preparedStatement.setString(2, answer1);
            preparedStatement.setString(3, answer2);
            preparedStatement.setString(4, answer3);
            preparedStatement.setString(5, answer4);
            preparedStatement.setInt(6, answerCorrect);
            preparedStatement.setString(7, subjectId);

            int x = preparedStatement.executeUpdate();
            if (x > 0) {
                result = true;
            }
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static boolean updateQuestion(String questionContent, String answer1, String answer2, String answer3, String answer4, int answerCorrect, String subjectId, int status, int questionId) throws ClassNotFoundException, SQLException, NamingException {
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            String sql = "update tblQuestions\n"
                    + "set questionContent = ?, answer1 = ?, answer2 = ?, answer3 = ?, answer4 = ?, answerCorrect = ?, subjectId = ?, status = ?\n"
                    + "where questionId = ?";
            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, questionContent);
            preparedStatement.setString(2, answer1);
            preparedStatement.setString(3, answer2);
            preparedStatement.setString(4, answer3);
            preparedStatement.setString(5, answer4);
            preparedStatement.setInt(6, answerCorrect);
            preparedStatement.setString(7, subjectId);
            preparedStatement.setInt(8, status);
            preparedStatement.setInt(9, questionId);

            int x = preparedStatement.executeUpdate();
            if (x > 0) {
                result = true;
            }
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static boolean deleteQuestion(int questionId) throws ClassNotFoundException, SQLException, NamingException {
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            String sql = "update tblQuestions set status = 0 where questionId = ? and status = 1";
            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, questionId);
            int x = preparedStatement.executeUpdate();
            if (x > 0) {
                result = true;
            }
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static Vector<QuestionDTO> getQuizQuestions(String subjectId, String subjectName, int numberOfQuestions) throws ClassNotFoundException, SQLException, NamingException {
        Vector<QuestionDTO> result = new Vector<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql
                    = "select top " + numberOfQuestions + "\n"
                    + "questionId, questionContent, answer1, answer2, answer3, answer4, answerCorrect, createdAt, tblQuestions.status as questionStatus\n"
                    + "from tblQuestions \n"
                    + "where subjectId = ? and tblQuestions.status = '1'\n"
                    + "order by newid()";

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, subjectId);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                int questionId = resultSet.getInt(1);
                String questionContent = resultSet.getString(2);
                String answer1 = resultSet.getString(3);
                String answer2 = resultSet.getString(4);
                String answer3 = resultSet.getString(5);
                String answer4 = resultSet.getString(6);
                int answerCorrect = resultSet.getInt(7);
                Date createdAt = resultSet.getDate(8);
                int questionStatus = resultSet.getInt(9);

                QuestionDTO dto = new QuestionDTO(questionId, questionContent, answer1, answer2, answer3, answer4, answerCorrect, createdAt, subjectId, subjectName, questionStatus, 1);
                result.add(dto);
            }
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }
}
