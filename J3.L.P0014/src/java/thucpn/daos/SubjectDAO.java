package thucpn.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import javax.naming.NamingException;
import thucpn.dtos.SubjectDTO;
import thucpn.utilities.DatabaseHelper;

public class SubjectDAO implements Serializable {

    public static Vector<SubjectDTO> getSubjects() throws SQLException, NamingException {
        Vector<SubjectDTO> result = new Vector<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql = "select subjectId, name, numberOfQuestions, time, status from tblSubjects where status = 1";

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String subjectId = resultSet.getString(1);
                String name = resultSet.getString(2);
                int numberOfQuestions = resultSet.getInt(3);
                int time = resultSet.getInt(4);
                int status = resultSet.getInt(5);
                SubjectDTO subjectDTO = new SubjectDTO(subjectId, name, numberOfQuestions, time, status);
                result.add(subjectDTO);
            }

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }
}
