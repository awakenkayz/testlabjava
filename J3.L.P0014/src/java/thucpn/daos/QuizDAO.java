package thucpn.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;
import javax.naming.NamingException;
import thucpn.dtos.QuizDTO;
import thucpn.dtos.QuizDetailDTO;
import thucpn.utilities.DatabaseHelper;

public class QuizDAO implements Serializable {

    public static boolean createQuiz(String quizId, String subjectId, int userId) throws ClassNotFoundException, SQLException, NamingException {
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            String sql = "insert into tblQuizs(quizId, subjectId, createdAt, userId)\n"
                    + "values(?, ?, GETDATE(), ?)";
            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, quizId);
            preparedStatement.setString(2, subjectId);
            preparedStatement.setInt(3, userId);
            int x = preparedStatement.executeUpdate();
            if (x > 0) {
                result = true;
            }
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static boolean submit(QuizDTO quiz, Vector<QuizDetailDTO> quizDetails) throws ClassNotFoundException, SQLException, NamingException {
        boolean result = true;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sql;

        try {
            connection = DatabaseHelper.makeConnection();

            // start transaction 
            connection.setAutoCommit(false);

            // update quiz result
            sql = "update tblQuizs\n"
                    + "set result = ?, submitAt = GETDATE()\n"
                    + "where quizId = ?";

            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setFloat(1, quiz.getResult());
            preparedStatement.setString(2, quiz.getQuizId());
            preparedStatement.executeUpdate();

            // insert quizDetails
            sql = "insert into tblQuizDetails(quizId, questionId, selectedAnswer, isCorrect)\n"
                    + "values(?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(sql);
            for (QuizDetailDTO quizDetail : quizDetails) {
                preparedStatement.setString(1, quizDetail.getQuizId());
                preparedStatement.setInt(2, quizDetail.getQuestionId());
                preparedStatement.setInt(3, quizDetail.getSelectedAnswer());
                preparedStatement.setBoolean(4, quizDetail.getIsCorrect());
                int rowEffects = preparedStatement.executeUpdate();

                if (rowEffects <= 0) {
                    throw new Exception("Something wrong");
                }
            }

            // end transaction
            connection.commit();

        } catch (Exception e) {
            result = false;
            if (connection != null) {
                connection.rollback();
            }
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.setAutoCommit(true);
                connection.close();
            }
        }

        return result;
    }
}
