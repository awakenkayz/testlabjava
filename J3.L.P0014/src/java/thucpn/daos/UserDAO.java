package thucpn.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Vector;
import javax.naming.NamingException;
import thucpn.dtos.HistoryDTO;
import thucpn.dtos.HistoryLineDTO;
import thucpn.dtos.UserDTO;
import thucpn.utilities.DatabaseHelper;

public class UserDAO implements Serializable {

    public static boolean register(String name, String email, String password)
            throws ClassNotFoundException, SQLException, NamingException {
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            String sql = "insert into tblUsers(roleId, name, email, password) \n"
                    + "values('2', ?, ?, HASHBYTES('SHA2_256', ?))";

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, password);
            int x = preparedStatement.executeUpdate();

            if (x > 0) {
                result = true;
            }

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static String checkLogin(String email, String password) throws ClassNotFoundException, SQLException, NamingException {
        String result = "";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql = "select tblRoles.name from tblUsers, tblRoles\n"
                    + "where tblRoles.roleId = tblUsers.roleId and status=1 and \n"
                    + "email=? and password=HASHBYTES('SHA2_256', ?)";

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getString(1);
            }

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static UserDTO getUserByEmail(String emailInput) throws ClassNotFoundException, SQLException, NamingException {
        UserDTO result = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql = "select userId, tblRoles.roleId, tblRoles.name, tblUsers.name, email from tblUsers, tblRoles\n"
                    + "where \n"
                    + "tblRoles.roleId = tblUsers.roleId and\n"
                    + "email = ?";
            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, emailInput);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int userId = resultSet.getInt(1);
                int roleId = resultSet.getInt(2);
                String roleName = resultSet.getString(3);
                String name = resultSet.getString(4);
                String email = resultSet.getString(5);

                result = new UserDTO(userId, roleId, roleName, name, email, "***", 1);
            }
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return result;
    }

    public static boolean checkEmailExisted(String email) throws ClassNotFoundException, SQLException, NamingException {
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql = "select userId from tblUsers\n"
                    + "where email = ?";
            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = true;
            }

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    public static Vector<HistoryDTO> getHistory(int page, int userId, String searchSubjectName) throws ClassNotFoundException, SQLException, NamingException {
        Vector<HistoryDTO> historys = new Vector<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql = "SELECT \n"
                    + "quizId, result, createdAt, submitAt, name,\n"
                    + "questionContent, answer1, answer2, answer3, answer4, selectedAnswer, answerCorrect, isCorrect\n"
                    + "FROM \n"
                    + "(\n"
                    + "select \n"
                    + "tblQuizs.quizId, result, tblQuizs.createdAt, submitAt, tblSubjects.name,\n"
                    + "questionContent, answer1, answer2, answer3, answer4, selectedAnswer, answerCorrect, isCorrect,\n"
                    + "ROW_NUMBER() OVER (ORDER BY tblQuizs.createdAt) as row \n"
                    + "from tblSubjects, tblQuizs, tblQuizDetails, tblQuestions\n"
                    + "where\n"
                    + "tblSubjects.subjectId = tblQuizs.subjectId and tblQuizs.quizId = tblQuizDetails.quizId and tblQuizDetails.questionId = tblQuestions.questionId and\n"
                    + "tblQuizs.userId = ? and\n"
                    + "tblSubjects.name like ?\n"
                    + ")\n"
                    + "a \n"
                    + "WHERE row >= ? and row <= ?";

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, "%" + searchSubjectName + "%");
            preparedStatement.setInt(3, (page - 1) * 20 + 1);
            preparedStatement.setInt(4, page * 20);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String quizId = resultSet.getString(1);
                float result = resultSet.getFloat(2);
                Date createdAt = resultSet.getDate(3);
                Date submitAt = resultSet.getDate(4);
                String subjectName = resultSet.getString(5);
                String questionContent = resultSet.getString(6);
                String answer1 = resultSet.getString(7);
                String answer2 = resultSet.getString(8);
                String answer3 = resultSet.getString(9);
                String answer4 = resultSet.getString(10);
                int selectedAnswer = resultSet.getInt(11);
                int answerCorrect = resultSet.getInt(12);
                boolean isCorrect = resultSet.getBoolean(13);

                HistoryLineDTO historyLine
                        = new HistoryLineDTO(questionContent, answer1, answer2, answer3, answer4, selectedAnswer, answerCorrect, isCorrect);

                HistoryDTO his = getExist(historys, quizId);

                if (his == null) {
                    Vector<HistoryLineDTO> historyLines = new Vector<>();
                    historyLines.add(historyLine);
                    historys.add(new HistoryDTO(quizId, result, createdAt, submitAt, subjectName, historyLines));
                } else {
                    his.getHistoryDetails().add(historyLine);
                }
            }

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        return historys;
    }

    public static int getNumberOfHistoryLine(int userId, String searchSubjectName) throws SQLException, NamingException {
        int result = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            String sql
                    = "select \n"
                    + "count(tblQuizs.quizId)\n"
                    + "from tblSubjects, tblQuizs, tblQuizDetails, tblQuestions\n"
                    + "where\n"
                    + "tblSubjects.subjectId = tblQuizs.subjectId and tblQuizs.quizId = tblQuizDetails.quizId and tblQuizDetails.questionId = tblQuestions.questionId and\n"
                    + "tblQuizs.userId = ? and\n"
                    + "tblSubjects.name like ?";

            connection = DatabaseHelper.makeConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, "%" + searchSubjectName + "%");
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

        return result;
    }

    private static HistoryDTO getExist(Vector<HistoryDTO> historys, String quizId) {
        for (HistoryDTO history : historys) {
            if (history.getQuizId().equals(quizId)) {
                return history;
            }
        }
        return null;
    }
}
