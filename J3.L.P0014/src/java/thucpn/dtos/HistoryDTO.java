package thucpn.dtos;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;

public class HistoryDTO implements Serializable {

    private String quizId;
    private float result;
    private Date createdAt;
    private Date submitAt;
    private String subjectName;
    private Vector<HistoryLineDTO> historyDetails;

    public HistoryDTO() {
    }

    public HistoryDTO(String quizId, float result, Date createdAt, Date submitAt, String subjectName, Vector<HistoryLineDTO> historyDetails) {
        this.quizId = quizId;
        this.result = result;
        this.createdAt = createdAt;
        this.submitAt = submitAt;
        this.subjectName = subjectName;
        this.historyDetails = historyDetails;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getSubmitAt() {
        return submitAt;
    }

    public void setSubmitAt(Date submitAt) {
        this.submitAt = submitAt;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Vector<HistoryLineDTO> getHistoryDetails() {
        return historyDetails;
    }

    public void setHistoryDetails(Vector<HistoryLineDTO> historyDetails) {
        this.historyDetails = historyDetails;
    }

}
