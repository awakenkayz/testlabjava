package thucpn.dtos;

import java.io.Serializable;

public class HistoryLineDTO implements Serializable {

    private String questionContent;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private int selectedAnswer;
    private int answerCorrect;
    private boolean isCorrect;

    public HistoryLineDTO() {
    }

    public HistoryLineDTO(String questionContent, String answer1, String answer2, String answer3, String answer4, int selectedAnswer, int answerCorrect, boolean isCorrect) {
        this.questionContent = questionContent;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.selectedAnswer = selectedAnswer;
        this.answerCorrect = answerCorrect;
        this.isCorrect = isCorrect;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public int getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(int selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public int getAnswerCorrect() {
        return answerCorrect;
    }

    public void setAnswerCorrect(int answerCorrect) {
        this.answerCorrect = answerCorrect;
    }

    public boolean isIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

}
