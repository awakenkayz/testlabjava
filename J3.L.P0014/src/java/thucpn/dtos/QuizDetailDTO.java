package thucpn.dtos;

import java.io.Serializable;

public class QuizDetailDTO implements Serializable {

    private int quizDetailId;
    private String quizId;
    private int questionId;
    private int selectedAnswer;
    private boolean isCorrect;

    public QuizDetailDTO() {
    }

    public QuizDetailDTO(int quizDetailId, String quizId, int questionId, int selectedAnswer, boolean isCorrect) {
        this.quizDetailId = quizDetailId;
        this.quizId = quizId;
        this.questionId = questionId;
        this.selectedAnswer = selectedAnswer;
        this.isCorrect = isCorrect;
    }

    public int getQuizDetailId() {
        return quizDetailId;
    }

    public void setQuizDetailId(int quizDetailId) {
        this.quizDetailId = quizDetailId;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getSelectedAnswer() {
        return selectedAnswer;
    }

    public void setSelectedAnswer(int selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

}
