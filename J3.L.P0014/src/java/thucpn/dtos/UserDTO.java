package thucpn.dtos;

import java.io.Serializable;

public class UserDTO implements Serializable {

    private int userId;
    private int roleId;
    private String roleName;
    private String name;
    private String email;
    private String password;
    private int status;

    public UserDTO() {
    }

    public UserDTO(int userId, int roleId, String roleName, String name, String email, String password, int status) {
        this.userId = userId;
        this.roleId = roleId;
        this.roleName = roleName;
        this.name = name;
        this.email = email;
        this.password = password;
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
