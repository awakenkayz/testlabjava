package thucpn.dtos;

import java.io.Serializable;

public class SubjectDTO implements Serializable {

    private String subjectId;
    private String name;
    private int numberOfQuestions;
    private int time;
    private int status;

    public SubjectDTO() {
    }

    public SubjectDTO(String subjectId, String name, int numberOfQuestions, int time, int status) {
        this.subjectId = subjectId;
        this.name = name;
        this.numberOfQuestions = numberOfQuestions;
        this.time = time;
        this.status = status;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfQuestions() {
        return numberOfQuestions;
    }

    public void setNumberOfQuestions(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
