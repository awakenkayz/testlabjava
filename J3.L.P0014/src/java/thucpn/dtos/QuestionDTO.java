package thucpn.dtos;

import java.io.Serializable;
import java.util.Date;

public class QuestionDTO implements Serializable {

    private int questionId;
    private String questionContent;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private int answerCorrect;
    private Date createdAt;
    private String subjectId;
    private String subjectName;
    private int questionStatus;
    private int subjectStatus;

    public QuestionDTO() {
    }

    public QuestionDTO(int questionId, String questionContent, String answer1, String answer2, String answer3, String answer4, int answerCorrect, Date createdAt, String subjectId, String subjectName, int questionStatus, int subjectStatus) {
        this.questionId = questionId;
        this.questionContent = questionContent;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.answerCorrect = answerCorrect;
        this.createdAt = createdAt;
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.questionStatus = questionStatus;
        this.subjectStatus = subjectStatus;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }

    public int getAnswerCorrect() {
        return answerCorrect;
    }

    public void setAnswerCorrect(int answerCorrect) {
        this.answerCorrect = answerCorrect;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getQuestionStatus() {
        return questionStatus;
    }

    public void setQuestionStatus(int questionStatus) {
        this.questionStatus = questionStatus;
    }

    public int getSubjectStatus() {
        return subjectStatus;
    }

    public void setSubjectStatus(int subjectStatus) {
        this.subjectStatus = subjectStatus;
    }

}
