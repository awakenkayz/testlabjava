package thucpn.dtos;

import java.io.Serializable;
import java.util.Date;

public class QuizDTO implements Serializable {

    private String quizId;
    private String subjectId;
    private Date createdAt;
    private float result;
    private Date submitAt;
    private int userId;

    public QuizDTO() {
    }

    public QuizDTO(String quizId, String subjectId, Date createdAt, float result, Date submitAt, int userId) {
        this.quizId = quizId;
        this.subjectId = subjectId;
        this.createdAt = createdAt;
        this.result = result;
        this.submitAt = submitAt;
        this.userId = userId;
    }

    public String getQuizId() {
        return quizId;
    }

    public void setQuizId(String quizId) {
        this.quizId = quizId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public float getResult() {
        return result;
    }

    public void setResult(float result) {
        this.result = result;
    }

    public Date getsubmitAt() {
        return submitAt;
    }

    public void setsubmitAt(Date submitAt) {
        this.submitAt = submitAt;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
